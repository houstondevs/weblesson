from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as auth_views

from accounts.views import registration, profile, MeProfile, users_list

from courses.views import new_post

from days.views import set_day

from .views import main_page
from . import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', main_page, name='main_page'),
    path('login/', auth_views.LoginView.as_view(template_name='accounts/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/login/'), name='logout'),
    path('registration/teacher/', registration, name='teacher_registration'),
    path('registration/student/', registration, name='student_registration'),
    path('user/<int:pk>/', profile, name="user_profile"),
    path('me/', MeProfile.as_view(), name='me_profile'),
    path('users/', users_list, name='users'),
    path('newpost/', new_post, name="new_post"),
    path('add_date/', set_day, name='new_date')
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)