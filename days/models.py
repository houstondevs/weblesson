from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Days(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Преподователь", related_name='user')
    day = models.DateTimeField()

    def __str__(self):
        return str(self.day)