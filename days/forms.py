import datetime

from django import forms
from .models import Days


class DaysForm(forms.ModelForm):
    day = forms.DateTimeField(required=False, widget=forms.DateInput(attrs={'type': 'datetime-local'}),
                                     initial=format(datetime.date.today(),'%Y-%m-%dT%H:%M'), localize=True, input_formats=['%Y-%m-%dT%H:%M'])

    class Meta:
        model = Days
        fields =['day']