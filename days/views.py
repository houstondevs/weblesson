from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import DaysForm
from django.contrib.auth.decorators import login_required
from .models import Days


@login_required()
def set_day(request):
    if request.method == 'POST':
        form = DaysForm(data=request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            return HttpResponseRedirect('/me')
    else:
        form = DaysForm()
        return render(request, 'set_date.html', context={'form':form})