from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from .forms import PostForm


@login_required
def new_post(request):
    if request.method == 'POST':
        post_form = PostForm(request.POST, request.FILES)
        if post_form.is_valid():
            instance = post_form.save(commit=False)
            instance.created_by = request.user
            instance.save()
            return HttpResponseRedirect('/me')
    else:
        post_form = PostForm()
        return render(request, 'accounts/new_post.html', context={'post':post_form})