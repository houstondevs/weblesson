from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Post(models.Model):
    title = models.CharField(max_length=50, verbose_name="Тема")
    text = models.TextField(max_length=1000, verbose_name="Текст")
    file = models.FileField(upload_to='files', blank=True, null=True, verbose_name="Файл")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Автор", related_name="user_post")