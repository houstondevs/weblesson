from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    file = forms.FileField(label='File', required=False, error_messages={'invalid': ("files only")},
                             widget=forms.FileInput)
    class Meta:
        model = Post
        fields = ("title", "text", "file")