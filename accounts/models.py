from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.shortcuts import reverse

from university.models import University


class UserManager(BaseUserManager):
    def _create_user(self, email, first_name, last_name, middle_name, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, first_name=first_name, last_name=last_name, middle_name=middle_name, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, first_name, last_name, middle_name, university, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('university', university)
        extra_fields.setdefault('is_student', True)
        return self._create_user(email, first_name, last_name, middle_name, password, **extra_fields)

    def create_teacher(self, email, first_name, last_name, middle_name, university, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        extra_fields.setdefault('university', university)
        extra_fields.setdefault('is_teacher', True)

        return self._create_user(email, first_name, last_name, middle_name, password, **extra_fields)

    def create_superuser(self, email, first_name, last_name, middle_name, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('university', University.objects.get(id=1))

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, first_name, last_name, middle_name, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    email = models.EmailField(
        verbose_name='Почтовый адрес',
        unique=True,
        max_length=60,
        error_messages={'unique':'Пользователь с таким адресом уже зарегистрирован!'}
    )
    image = models.ImageField(default='default.png', upload_to='avatars')
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)
    middle_name = models.CharField(verbose_name='Отчество', max_length=50)
    university = models.ForeignKey(verbose_name='Учебное заведение', on_delete=models.PROTECT, to=University)
    is_student = models.BooleanField(verbose_name="Статус студента!", default=False)
    is_teacher = models.BooleanField(verbose_name="Статус преподователя!", default=False)

    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['first_name', 'last_name', 'middle_name']

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_status(self):
        if self.is_student:
            return 'Студент'
        elif self.is_teacher:
            return 'Преподователь'
        elif self.is_staff:
            return 'Администратор'

    def get_full_name(self):
        full_name = '%s %s %s' % (self.last_name, self.first_name, self.middle_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def get_absolute_url(self):
        return reverse('user_profile', args=(self.id,))


#u=User.objects.create_user(email="artem@artem.ru", first_name="Artem", last_name="Mukhachev", middle_name="Pavlovich", university=v, password="142536asd")
