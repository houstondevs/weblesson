from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.views import View
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator

from .forms import CustomUserCreationForm, CustomUserChangeForm

from university.models import University
from courses.models import Post

from days.models import Days

User = get_user_model()


def registration(request):
    if request.user.is_authenticated:
        return redirect('me_profile')
    if request.method == 'POST':
        reg_form = CustomUserCreationForm(data=request.POST)
        if reg_form.is_valid():
            university = University.objects.get(id=reg_form.data.get('university'))
            if request.get_full_path() == '/registration/teacher/':
                user = User.objects.create_teacher(
                    email=reg_form.data.get('email'),
                    first_name=reg_form.data.get('first_name'),
                    last_name=reg_form.data.get('last_name'),
                    middle_name=reg_form.data.get('middle_name'),
                    university=university,
                    password=reg_form.data.get('password1')
                )
                user.save()
            elif request.get_full_path() == '/registration/student/':
                user = User.objects.create_user(
                    email=reg_form.data.get('email'),
                    first_name=reg_form.data.get('first_name'),
                    last_name=reg_form.data.get('last_name'),
                    middle_name=reg_form.data.get('middle_name'),
                    university=university,
                    password=reg_form.data.get('password1')
                )
                user.save()
            return redirect('main_page')
    else:
        reg_form = CustomUserCreationForm()
    return render(request, 'accounts/registration.html', {'registration_form': reg_form})


def profile(request, pk):
    user = get_object_or_404(User, pk=pk)
    posts = Post.objects.filter(created_by=user)
    dates = Days.objects.filter(user=user)
    if user == request.user:
        return redirect('me_profile')
    return render(request, 'accounts/profiles.html',  context={'user_info':user, 'posts':posts, 'dates':dates})


class MeProfile(LoginRequiredMixin, View):
    template_name = 'accounts/my_profile.html'

    def get(self, request):
        user = request.user
        posts = Post.objects.filter(created_by=user)
        dates = Days.objects.filter(user=user)
        update_form = CustomUserChangeForm(instance=user)
        return render(request, self.template_name, context={'user_me':user, 'posts':posts, 'update_form':update_form, 'dates':dates})

    def post(self, request):
        user = request.user
        update_form = CustomUserChangeForm(request.POST, request.FILES, instance=user)
        if update_form.is_valid():
            user = update_form.save()
            user.save()
            return HttpResponseRedirect('/me')
        else:
            return redirect('me_profile')


def users_list(request):
    users = User.objects.all()
    paginator = Paginator(users, 8)
    page = request.GET.get('page')
    users = paginator.get_page(page)
    return render(request, 'accounts/users_list.html', context={'users':users})