from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms


User = get_user_model()


class CustomUserCreationForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(), label='Пароль')
    password2 = forms.CharField(widget=forms.PasswordInput(), label='Повторите пароль')

    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'middle_name', 'university', 'password1', 'password2')

    def clean(self):
        cleaned_data = super(CustomUserCreationForm, self).clean()
        p1 = cleaned_data.get('password1')
        p2 = cleaned_data.get('password2')

        if p1 != p2:
            raise forms.ValidationError('Пароли не совпадают!')


class CustomUserChangeForm(UserChangeForm):
    password = None
    image = forms.ImageField(label='Image', required=False, error_messages={'invalid': ("Image files only")},
                             widget=forms.FileInput)

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'middle_name', 'university', 'image']
