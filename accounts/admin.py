from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin


User = get_user_model()


class CustomUserAdmin(UserAdmin):
    model = User
    list_display = ('email', 'is_staff', 'is_active', 'is_student', 'is_teacher')
    list_filter = ('email', 'is_staff', 'is_active', 'is_student', 'is_teacher')
    fieldsets = (
        ('Информация о пользователе', {'fields': ('email', 'password', 'first_name', 'last_name', 'middle_name', 'university', 'image')}),
        ('Статус', {'fields': ('is_teacher', 'is_student')}),
        ('Разрешения', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'middle_name', 'university', 'password1', 'password2', 'is_staff', 'is_active', 'is_student', 'is_teacher')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)


admin.site.register(User, CustomUserAdmin)

