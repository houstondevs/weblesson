from __future__ import unicode_literals
import json
import os

from django.db import migrations, models

DATA_PATH = os.path.join(os.path.dirname(__file__), 'data')


def forwards_func(apps, schema_editor):
    migrate_fs_type_position(apps, schema_editor, forward=True)


def reverse_func(apps, schema_editor):
    migrate_fs_type_position(apps, schema_editor, forward=False)


class Migration(migrations.Migration):
    dependencies = [
        ('university', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]


def migrate_fs_type_position(apps, schema_editor, forward=True):
    Universities = apps.get_model("university", "University")

    data_list = []
    with open(DATA_PATH + '/universities.jsonl', 'rt') as jsonl_file:
        for line in jsonl_file:
            id_, name = read_jsonl(line)
            data = dict(id=id_, name=name)
            data_list.append(data)

    if forward:
        for data in data_list:
            university, cuniversity = Universities.objects.get_or_create(
                id=data['id'],
                name=data['name']
            )
            university.save()


def read_jsonl(line):
    return json.loads(line)
