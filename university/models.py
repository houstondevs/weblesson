from django.db import models


class University(models.Model):
    class Meta:
        verbose_name = 'Университет'
        verbose_name_plural = 'Университеты'

    name = models.CharField(verbose_name='Название университета', unique=True, max_length=255)

    def __str__(self):
        return self.name
